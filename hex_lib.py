import math


class Point(tuple):
    def __new__(cls, x, y):
        return tuple.__new__(cls, (x, y))

    def __getnewargs__(self):
        return self.x, self.y

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Hex(tuple):
    def __new__(cls, q, r, s):
        return tuple.__new__(cls, (q, r, s))

    def __getnewargs__(self):
        return self.q, self.r, self.s

    def __init__(self, q, r, s):
        self.q = q
        self.r = r
        self.s = s
        assert not (round(q + r + s) != 0), "q + r + s must be 0"

    def __add__(self, other):
        return Hex(self.q + other.q, self.r + other.r, self.s + other.s)

    def __sub__(self, other):
        return Hex(self.q - other.q, self.r - other.r, self.s - other.s)

    def __mul__(self, scale):
        return Hex(self.q * scale, self.r * scale, self.s * scale)

    def hex_rotate_left(self):
        return Hex(-self.s, -self.q, -self.r)

    def hex_rotate_right(self):
        return Hex(-self.r, -self.s, -self.q)


HEX_DIRECTIONS = [Hex(1, 0, -1),
                  Hex(1, -1, 0),
                  Hex(0, -1, 1),
                  Hex(-1, 0, 1),
                  Hex(-1, 1, 0),
                  Hex(0, 1, -1)]

HEX_DIAGONALS = [Hex(2, -1, -1),
                 Hex(1, -2, 1),
                 Hex(-1, -1, 2),
                 Hex(-2, 1, 1),
                 Hex(-1, 2, -1),
                 Hex(1, 1, -2)]


def hex_neighbor(hx1, direction):
    hx2 = HEX_DIRECTIONS[direction]
    adj_hex = hx1 + hx2
    return adj_hex


def hex_diagonal_neighbor(hx1, direction):
    hx2 = HEX_DIAGONALS[direction]
    diag_hex = hx1 + hx2
    return diag_hex


def hex_length(hx1):
    return (abs(hx1.q) + abs(hx1.r) + abs(hx1.s)) // 2


def hex_distance(hx1, hx2):
    return hex_length(Hex.__sub__(hx1, hx2))


def hex_round(hx1):
    qi = int(round(hx1.q))
    ri = int(round(hx1.r))
    si = int(round(hx1.s))
    q_diff = abs(qi - hx1.q)
    r_diff = abs(ri - hx1.r)
    s_diff = abs(si - hx1.s)
    if q_diff > r_diff and q_diff > s_diff:
        qi = -ri - si
    else:
        if r_diff > s_diff:
            ri = -qi - si
        else:
            si = -qi - ri
    return Hex(qi, ri, si)


def hex_lerp(hx1, hx2, t):
    return Hex(hx1.q * (1.0 - t) + hx2.q * t,
               hx1.r * (1.0 - t) + hx2.r * t,
               hx1.s * (1.0 - t) + hx2.s * t)


def hex_linedraw(hx1, hx2):
    N = hex_distance(hx1, hx2)
    a_nudge = Hex(hx1.q + 0.000001, hx1.r + 0.000001, hx1.s - 0.000002)
    b_nudge = Hex(hx2.q + 0.000001, hx2.r + 0.000001, hx2.s - 0.000002)
    results = []
    step = 1.0 / max(N, 1)
    for i in range(0, N + 1):
        results.append(hex_round(hex_lerp(a_nudge, b_nudge, step * i)))
    return results


class OffsetCoord(tuple):
    def __new__(self, col, row):
        return tuple.__new__(self, (col, row))

    def __init__(self, col, row):
        self.col = col
        self.row = row


def qoffset_from_cube(hx1, offset):
    col = hx1.q
    row = hx1.r + (hx1.q + offset * (hx1.q & 1)) // 2
    return OffsetCoord(col, row)


def qoffset_to_cube(hx1, offset):
    q = hx1.col
    r = hx1.row - (hx1.col + offset * (hx1.col & 1)) // 2
    s = -q - r
    return Hex(q, r, s)


def roffset_from_cube(hx1, offset):
    col = hx1.q + (hx1.r + offset * (hx1.r & 1)) // 2
    row = hx1.r
    return OffsetCoord(col, row)


def roffset_to_cube(hx1, offset):
    q = hx1.col - (hx1.row + offset * (hx1.row & 1)) // 2
    r = hx1.row
    s = -q - r
    return Hex(q, r, s)


class DoubledCoord():
    def __init__(self, col, row):
        self.col = col
        self.row = row


def qdoubled_from_cube(hx1):
    col = hx1.q
    row = 2 * hx1.r + hx1.q
    return DoubledCoord(col, row)


def qdoubled_to_cube(hx1):
    q = hx1.col
    r = (hx1.row - hx1.col) // 2
    s = -q - r
    return Hex(q, r, s)


def rdoubled_from_cube(hx1):
    col = 2 * hx1.q + hx1.r
    row = hx1.r
    return DoubledCoord(col, row)


def rdoubled_to_cube(hx1):
    q = (hx1.col - hx1.row) // 2
    r = hx1.row
    s = -q - r
    return Hex(q, r, s)


class Orientation():
    def __init__(self, f0, f1, f2, f3, b0, b1, b2, b3, start_angle):
        self.f0 = f0
        self.f1 = f1
        self.f2 = f2
        self.f3 = f3
        self.b0 = b0
        self.b1 = b1
        self.b2 = b2
        self.b3 = b3
        self.start_angle = start_angle


class Layout():
    def __init__(self, orientation, size, origin):
        self.orientation = orientation
        self.size = size
        self.origin = origin


LAYOUT_POINTY = Orientation(math.sqrt(3.0),
                            math.sqrt(3.0) / 2.0,
                            0.0,
                            3.0 / 2.0,
                            math.sqrt(3.0) / 3.0,
                            -1.0 / 3.0,
                            0.0,
                            2.0 / 3.0,
                            0.5)

LAYOUT_FLAT = Orientation(3.0 / 2.0,
                          0.0,
                          math.sqrt(3.0) / 2.0,
                          math.sqrt(3.0),
                          2.0 / 3.0,
                          0.0,
                          -1.0 / 3.0,
                          math.sqrt(3.0) / 3.0,
                          0.0)


def hex_to_pixel(layout, hx1):
    M = layout.orientation
    size = layout.size
    origin = layout.origin
    x = (M.f0 * hx1.q + M.f1 * hx1.r) * size.x
    y = (M.f2 * hx1.q + M.f3 * hx1.r) * size.y
    return Point(x + origin.x, y + origin.y)


def pixel_to_hex(layout, p):
    M = layout.orientation
    size = layout.size
    origin = layout.origin
    pt = Point((p.x - origin.x) / size.x, (p.y - origin.y) / size.y)
    q = M.b0 * pt.x + M.b1 * pt.y
    r = M.b2 * pt.x + M.b3 * pt.y
    return Hex(q, r, -q - r)


def hex_corner_offset(layout, corner):
    M = layout.orientation
    size = layout.size
    angle = 2.0 * math.pi * (M.start_angle - corner) / 6.0
    return Point(size.x * math.cos(angle), size.y * math.sin(angle))


def polygon_corners(layout, hx1):
    corners = []
    center = hex_to_pixel(layout, hx1)
    for i in range(0, 6):
        offset = hex_corner_offset(layout, i)
        corners.append(Point(center.x + offset.x, center.y + offset.y))
    return corners
