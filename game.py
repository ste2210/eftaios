"""
Main game module, holds the current state of the game which can then be
requested by the client
"""
import random
import statistics as stats
import characters as chars
import zones


class Game():
    def __init__(self, game_id, size):
        self.game_id = game_id
        self.size = size
        self.map = zones.ZONE_DICT['galilei']
        self.players = {}
        self.moves = {}
        self.players_killed = []
        self.ready = False
        self.chars_assigned = False
        self.current_player = 0
        self.deck = self.create_deck()

    def create(self, player, name):
        self.players[player] = name

    def move(self, player, move):
        self.moves[player].append(move)
        return self.get_next_player()

    def attack(self, player, move):
        for plyr in self.moves:
            if plyr == player:
                continue
            for mov in self.moves[plyr]:
                if move == mov:
                    self.players_killed.append(plyr)
                    self.players[plyr] = chars.Alien('Standard Alien')
                    self.moves[plyr] = []
        return self.move(player, move), self.players_killed, self.players

    def create_char_deck(self):
        human_list = []
        alien_list = []
        human_count = self.size // 2
        alien_count = self.size - human_count
        while len(human_list) < human_count:
            human_choice = random.choice(list(chars.HUMAN_DICT.values()))
            if human_choice not in human_list:
                human_list.append(human_choice)
        while len(alien_list) < alien_count:
            alien_choice = random.choice(list(chars.ALIEN_DICT.values()))
            if alien_choice not in alien_list:
                alien_list.append(alien_choice)
        char_deck = human_list + alien_list
        return char_deck

    def assign_map(self, votes):
        try:
            self.map = stats.mode(votes)
        except stats.StatisticsError:
            self.map = random.choice(list(zones.ZONE_DICT.values))
        return self.map

    def assign_chars(self):
        char_deck = self.create_char_deck()
        random.shuffle(char_deck)
        for player in self.players:
            self.players[player] = char_deck.pop()
            self.moves[player] = []
        self.chars_assigned = True
        return self.players, self.chars_assigned, self.moves

    def get_next_player(self):
        self.current_player += 1
        if self.current_player == self.size:
            self.current_player = 0
        return self.current_player

    def create_deck(self):
        noise = [0] * 27
        fake_noise = [1] * 27
        silence = [2] * 6
        power = [3] * 17
        deck = noise + fake_noise + silence + power
        random.shuffle(deck)
        return deck

    def draw_card(self):
        random.shuffle(self.deck)
        card = self.deck.pop()
        return card
