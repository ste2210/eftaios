"""
button.py contains the Button class that handles operation involving drawing
and using buttons
"""


class Button:
    """
    Takes 4 arguments plus 1 optional:
        pygame surface object that button should be drawn to
        tuple of coordinated for button's topleft corner
        pygame image of inactive button
        pygame image of highlighted button
        (optional) pygame image of button when clicked
    """

    def __init__(self, surface, pos, image1, image2=None):
        """
        Creates button object
        """
        self.surface = surface
        self.pos = pos
        self.button = image1
        self.colour_img = image2
        self.clicked = False

    def draw(self, mouse_pos, mouseclicked):
        """
        determines the state of the button and draws accordingly
        """
        bttn = self.rect()

        if self.colour_img:
            if bttn.collidepoint(mouse_pos):
                self.surface.blit(self.colour_img, self.pos)
                if mouseclicked:
                    self.clicked = not self.clicked
            elif self.clicked:
                self.surface.blit(self.colour_img, self.pos)
            else:
                self.surface.blit(self.button, self.pos)
        else:
            self.surface.blit(self.button, self.pos)

        return self.clicked

    def rect(self):
        """
        finds rect object containing button
        """
        return self.button.get_rect(topleft=self.pos)
