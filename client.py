"""
client.py handles all user-specific game details, primarily determining what
is to be drawn to the screen
"""
import sys
import pygame as pg
import pygame.locals as pl
import random
import hex_lib as hl
import text
from network import Network
from button import Button

# Set up visuals such as frames per second and screen size
FPS = 75
WINDOWWIDTH = 1150  # (board is 889px wide)
WINDOWHEIGHT = 748

# Global variables for defining layout of hexagonal grid
SIZE = hl.Point(25, 25)
ORIGIN = hl.Point(29, 134)
LAYOUT = hl.Layout(hl.LAYOUT_FLAT, SIZE, ORIGIN)

# Colour = (R, G, B, a)
RED = (255, 0, 0, 255)
GREEN = (0, 128, 0, 255)
BLUE = (0, 0, 255, 255)
BLACK = (0, 0, 0, 255)
WHITE = (255, 255, 255, 255)
GREY = (128, 128, 128, 160)
TRANS_RED = (255, 0, 0, 85)
TRANS_GREEN = (0, 255, 0, 85)
TRANS_BLUE = (0, 0, 255, 85)

# Images
ATTK_BTTN = pg.image.load('Images/attack_button.png')
ATTK_BTTN_H = pg.image.load('Images/attack_button_highlight.png')
BACK_BTTN = pg.image.load('Images/back_button.png')
BACK_BTTN_H = pg.image.load('Images/back_button_highlight.png')
QUIT_BTTN = pg.image.load('Images/quit_button.png')
QUIT_BTTN_H = pg.image.load('Images/quit_button_highlight.png')
RULE_BTTN = pg.image.load('Images/rules_button.png')
RULE_BTTN_H = pg.image.load('Images/rules_button_highlight.png')
STRT_BTTN = pg.image.load('Images/start_button.png')
STRT_BTTN_H = pg.image.load('Images/start_button_highlight.png')

# Initiate Pygame globals
pg.init()
pg.display.set_caption('Escape from the Aliens in Outer Space')
FPSCLOCK = pg.time.Clock()
DISPLAYSURF = pg.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
TRANS_SURF = DISPLAYSURF.convert_alpha()
HEX_SURF = TRANS_SURF


def main():
    """
    main game function: sets up network and game variables
    """
    name = None
    net = Network()
    player_id = int(net.player)

    while not name:
        name = title_screen()

    net.send(('create', name))
    game = net.send(('get', None))
    zone = game.map
    hand = []
    attk_bttn = Button(TRANS_SURF, (900, 300), ATTK_BTTN)

    while True:  # main game loop
        draw_window(zone)

        game = net.send(('get', None))
        active_player = game.current_player
        char = game.players[player_id]

        for event in pg.event.get():
            event_handler(event)

        attk_bttn.draw((0, 0), False)

        if game.chars_assigned:
            if char.species == 'human':
                start = zone.human_start
            elif char.species == 'alien':
                start = zone.alien_start

            visited = [start] + game.moves[player_id]
            board = get_board(zone)
            hex_board = create_hex_board(board)

            for hex_ in visited:
                draw_tile(hex_, char.color)

            if active_player == player_id:
                your_move(net, char, hex_board, zone, visited, hand, game)

            DISPLAYSURF.blit(TRANS_SURF, (0, 0))
            pg.display.update()
            FPSCLOCK.tick(FPS)
        else:
            pause_menu(net)



def event_handler(event):
    """
    Called whenever a mouse or key event is registered. Returns the state of
    the mouse (clicked and position), and keys pressed.
    """
    mouseclicked = False
    mousex = 0
    mousey = 0
    return_ = False

    if event.type == pl.QUIT or (event.type == pl.KEYUP and
                                 event.key == pl.K_ESCAPE):
        pg.quit()
        sys.exit()
    elif event.type == pl.MOUSEMOTION:
        mousex, mousey = event.pos
    elif event.type == pl.MOUSEBUTTONUP:
        mousex, mousey = event.pos
        mouseclicked = True
    elif event.type == pg.KEYDOWN:
        if event.key == pg.K_RETURN:
            return_ = True
    return mouseclicked, mousex, mousey, return_


def your_move(net, char, hex_board, zone, visited, hand, game):
    mousex = 0
    mousey = 0
    attk_bttn = Button(TRANS_SURF, (900, 300), ATTK_BTTN, ATTK_BTTN_H)

    while True:
        mouseclicked = False
        draw_window(zone)

        for event in pg.event.get():
            mouseclicked, mousex, mousey = event_handler(event)[:3]

        mouse_pos = hl.Point(mousex, mousey)
        selected_hex = get_hex_at_pixel(mouse_pos)[0]
        allowed_moves = get_allowed_moves(visited, hex_board, char.speed)

        for hex_ in visited:
            draw_tile(hex_, char.color)

        for hex_ in allowed_moves:
            draw_tile(hex_, TRANS_GREEN)

        attk_bttn.draw(mouse_pos, mouseclicked)

        if selected_hex in allowed_moves:
            draw_tile(selected_hex, char.color)
            if mouseclicked:
                command = 'attack' if attk_bttn.clicked else 'move'
                if selected_hex in zone.danger:
                    card = draw_card(game)
                    if card == 0:
                        declare_sector(selected_hex)
                    if card == 1:
                        declare_choice()
                    if card == 2:
                        declare_silence()
                    if card == 3:
                        declare_silence()
                        hand.append(card)
                net.send((command, selected_hex))
                attk_bttn.clicked = False
                break

        elif selected_hex in hex_board:
            draw_tile(selected_hex)

        DISPLAYSURF.blit(TRANS_SURF, (0, 0))
        pg.display.update()
        FPSCLOCK.tick(FPS)


def title_screen():
    mousex = 0
    mousey = 0
    mouseclicked = False
    DISPLAYSURF.fill(BLACK)

    for i in range(0, 255, 4):
        TRANS_SURF.fill(BLACK)
        pg.display.update()
        title = pg.image.load('Images/titlescreen.png').convert()
        title.set_alpha(i)
        TRANS_SURF.blit(title, (226, 70))
        DISPLAYSURF.blit(TRANS_SURF, (0, 0))
        pg.display.update()
        FPSCLOCK.tick(FPS)

    quit_bttn = Button(DISPLAYSURF, (161, 491), QUIT_BTTN, QUIT_BTTN_H)
    rule_bttn = Button(DISPLAYSURF, (427, 491), RULE_BTTN, RULE_BTTN_H)
    strt_bttn = Button(DISPLAYSURF, (693, 491), STRT_BTTN, STRT_BTTN_H)

    while True:
        for event in pg.event.get():
            mouseclicked, mousex, mousey = event_handler(event)[:3]

        mouse_pos = hl.Point(mousex, mousey)

        quit_bttn.draw(mouse_pos, mouseclicked)
        rule_bttn.draw(mouse_pos, mouseclicked)
        strt_bttn.draw(mouse_pos, mouseclicked)

        if quit_bttn.clicked:
            pg.quit()
            sys.exit()

        if rule_bttn.clicked:
            pass

        if strt_bttn.clicked:
            name = info_menu()
            return name

        pg.display.update()
        FPSCLOCK.tick(FPS)


def info_menu():
    mousex = 0
    mousey = 0
    mouseclicked = False
    return_ = False

    DISPLAYSURF.fill(BLACK)

    quit_bttn = Button(DISPLAYSURF, (161, 491), QUIT_BTTN, QUIT_BTTN_H)
    back_bttn = Button(DISPLAYSURF, (427, 491), BACK_BTTN, BACK_BTTN_H)
    strt_bttn = Button(DISPLAYSURF, (693, 491), STRT_BTTN, STRT_BTTN_H)

    enter_name = text.TextBox(WINDOWWIDTH / 2, 240, 60, 'ENTER YOUR NAME:')
    name = text.InputBox(WINDOWWIDTH / 2, 290, 500, 80, 90)

    while True:
        back_bttn.clicked = False

        for event in pg.event.get():
            mouseclicked, mousex, mousey, return_ = event_handler(event)
            name.handle_event(event)

        player_name = name.text

        mouse_pos = hl.Point(mousex, mousey)

        quit_bttn.draw(mouse_pos, mouseclicked)
        back_bttn.draw(mouse_pos, mouseclicked)
        strt_bttn.draw(mouse_pos, mouseclicked)

        enter_name.draw(DISPLAYSURF)
        name.draw(DISPLAYSURF)

        if quit_bttn.clicked:
            pg.quit()
            sys.exit()

        if back_bttn.clicked:
            return player_name

        if strt_bttn.clicked or return_:
            if not player_name:
                strt_bttn.clicked = return_ = False
            else:
                return player_name

        pg.display.update()
        FPSCLOCK.tick(FPS)


def vote_menu():
    return None


def pause_menu(net):
    net.send(('get', None))
    DISPLAYSURF.fill(BLACK)
    wait_message = text.TextBox(WINDOWWIDTH / 2, WINDOWHEIGHT / 2,
                                70, 'WAITING FOR PLAYERS...')
    wait_message.draw(DISPLAYSURF)
    pg.display.update()
    FPSCLOCK.tick(FPS)


def draw_window(zone):
    bg_image = pg.image.load(zone.image)
    DISPLAYSURF.blit(bg_image, (0, 0))
    TRANS_SURF.fill(WHITE)
    TRANS_SURF.blit(bg_image, (0, 0))


def draw_tile(hex_, color=GREY, line=0):
    pointlist = hl.polygon_corners(LAYOUT, hex_)
    pg.draw.polygon(TRANS_SURF, color, pointlist, line)


def get_board(zone):
    board = []
    for h_tile in range(23):
        for v_tile in range(14):
            tile = hl.OffsetCoord(h_tile, v_tile)
            if tile in zone.danger:
                board.append(tile)
            elif tile in zone.silent:
                board.append(tile)
            elif tile in zone.pod:
                board.append(tile)
    return board


def create_hex_board(board):
    hex_board = []
    offset = -1
    for tile in board:
        hex_ = hl.qoffset_to_cube(tile, offset)
        hex_board.append(hex_)
    return hex_board


def get_hex_at_pixel(pixel):
    offset = -1
    selected_hex = hl.hex_round(hl.pixel_to_hex(LAYOUT, pixel))
    selected_tile = hl.qoffset_from_cube(selected_hex, offset)
    selected_vertices = hl.polygon_corners(LAYOUT, selected_hex)
    return selected_hex, selected_tile, selected_vertices


def get_adjacent_hexes(selected_hex):
    adjacent_hexes = []
    for direction in range(6):
        adjacent_hexes.append(hl.hex_neighbor(selected_hex, direction))
    return adjacent_hexes


def get_allowed_moves(visited, hex_board, max_dist):
    prev_hex = visited[-1]
    ring1 = []
    ring2 = []
    ring3 = []
    ring1 += get_adjacent_hexes(prev_hex)
    allowed = list(set(ring1) & set(hex_board))
    if max_dist > 1:
        for hex_ in allowed:
            ring2 += get_adjacent_hexes(hex_)
        allowed += list(set(ring2) & set(hex_board))
        if max_dist > 2:
            for hex_ in allowed:
                ring3 += get_adjacent_hexes(hex_)
            allowed += list(set(ring3) & set(hex_board))
    allowed[:] = [move for move in allowed if move != prev_hex]
    return allowed

def draw_card(game):
    random.shuffle(game.deck)
    card = game.deck.pop()


def declare_sector(hex_):
    print('attacking sector', hex_)


def declare_choice():
    choice = input("Select a hex to declare")
    print('attacking sector', choice)


def declare_silence():
    print('silence in all sectors sector')


if __name__ == '__main__':
    main()
