class Human():
    def __init__(self, char):
        self.char = char
        self.species = 'human'
        self.speed = 1
        self.color = (0, 0, 255, 85)  # TRANS BLUE
        self.name = None


class Alien():
    def __init__(self, char):
        self.char = char
        self.species = 'alien'
        self.speed = 2
        self.color = (255, 0, 0, 85)  # TRANS_RED
        self.name = None


# Human Characters
HUMAN_DICT = {
    'Cptn': Human('The Captain'),
    'Pilot': Human('The Pilot'),
    'Psycho': Human('The Psychologist'),
    'Sldr': Human('The Soldier'),
    'Exec': Human('The Executive Officer'),
    'CoPi': Human('The Co-Pilot'),
    'Eng': Human('The Engineer'),
    'Med': Human('The Medic'),
}

# Alien Characters
ALIEN_DICT = {
    'Blnk': Alien('The Blink Alien'),
    'Silent': Alien('The Silent Alien'),
    'Surge': Alien('The Surge Alien'),
    'Brute': Alien('The Brute Alien'),
    'Inv': Alien('The Invisible Alien'),
    'Lurk': Alien('The Lurking Alien'),
    'Fast': Alien('The Fast Alien'),
    'Psych': Alien('The Psychic Alien'),
}
