"""
server.py handles transfer of game state to the relevant client
"""
import socket
import pickle
import _thread
from game import Game

HOSTNAME = 'FN2187'
HOST = '127.0.0.1'  # socket.gethostbyname(HOSTNAME)
PORT = 57344
SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
SOCK.bind((HOST, PORT))
SOCK.listen()
print('Server active, waiting for connections...')

LOBBY_SIZE = 2  # int(sys.argv[1])
GAMES = {}
PCOUNT = 0
PLAYER = 0


def threaded_client(conn, player, game_id):
    """
    Creates new thread for each player that establishes a connection with sever
    """
    global PCOUNT
    conn.send(pickle.dumps(player))

    game = GAMES.get(game_id)

    while True:
        try:
            data = pickle.loads(conn.recv(8192))
            command, info = data
            if command == 'reset':
                game.reset()
            elif command == 'create':
                print('Received message', data)
                game.create(player, info)
                print('player created:', game.players)
                if game.ready and len(game.players) == game.size:
                    game.assign_chars()
            elif command == 'move':
                print('Received message', data)
                game.move(player, info)
            elif command == 'attack':
                game.attack(player, info)
                print('Received message', data)
            elif command == 'get':
                pass
            elif command == 'card':
                game.draw_card(player, info)
                print('Received message', data)
            else:
                print('Command not recognised')
            conn.sendall(pickle.dumps(game))
        except (EOFError, ConnectionResetError) as err:
            print(err)
            break
    print('Lost connection')
    try:
        del GAMES[GAMEID]
        print("Closing Game", GAMEID)
    except KeyError:
        print('No game to be deleted')
    PCOUNT -= 1
    conn.close()


while True:

    CONN, ADDR = SOCK.accept()
    print('Connected to:', ADDR)

    PCOUNT += 1
    GAMEID = (PCOUNT - 1) // 2
    if PCOUNT % 2 == 1:
        GAMES[GAMEID] = Game(GAMEID, LOBBY_SIZE)
        print('Creating New Game...')
        PLAYER = 0
    else:
        GAMES[GAMEID].ready = True

    _thread.start_new_thread(threaded_client, (CONN, PLAYER, GAMEID))

    PLAYER += 1
