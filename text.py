import pygame as pg

# Colour (R, G, B)
BLUE = (20, 30, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


class TextBox():

    def __init__(self, x, y, size=45, txt=''):
        self.size = size
        self.text = str(txt)
        self.centre_x = x
        self.top_y = y
        self.colour = WHITE

    def generate_text_surface(self):
        font = pg.font.Font(None, self.size)
        text_surface = font.render(self.text, True, self.colour)
        text_rect = text_surface.get_rect()
        return text_surface, text_rect

    def get_box_topleft(self):
        text_surface, text_rect = self.generate_text_surface()
        min_box_width = max(text_rect.w, text_surface.get_width()+10)
        return (self.centre_x - (min_box_width / 2),
                self.top_y - (text_rect.h / 2))

    def draw(self, screen):
        text_surface = self.generate_text_surface()[0]
        screen.blit(text_surface, self.get_box_topleft())


class InputBox(TextBox):

    def __init__(self, x, y, width=300, height=40, size=45, txt=''):
        TextBox.__init__(self, x, y, size, txt)
        self.width = width
        self.height = height
        self.colour = BLUE

    def draw(self, screen):
        text_surface, text_rect = self.generate_text_surface()
        input_box = pg.Rect(self.centre_x, self.top_y, self.width, self.height)
        input_box.topleft = self.centre_x - (self.width / 2), self.top_y
        text_rect.center = input_box.center
        pg.draw.rect(screen, BLUE, input_box, 2)
        screen.fill(BLACK, input_box.inflate(-4, -4))
        screen.blit(text_surface, text_rect)

    def handle_event(self, event):
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_BACKSPACE:
                self.text = self.text[:-1]
            elif event.key == pg.K_RETURN:
                return
            else:
                self.text += event.unicode
